# README #

### What is this repository for? ###

This POC is for VueJS2+ webpack2 + Bootstrap + NodeJS (NPM) 

### How do I get set up? ###

Pre Req:

1. NodeJs 7.5
2. Atom -- IDE

### Contribution guidelines ###

Execute 'npm install' from the project folder to install required packages

Execute 'npm install -g webpack'

Execute 'npm install -g webpack-dev-server'

Execute 'webpack'

Execute 'webpack-dev-server'


### Who do I talk to? ###

Shahzad Akram or Tone Networks Team