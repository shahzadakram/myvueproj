Vue.config.debug = true;
Vue.config.devtools = true;

import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './App.vue';
import Users from './Users.vue';
import Home from './Home.vue';
import LoginHome from './LoginHome.vue';
import UserHome from './UserHome.vue';
import Player from './Player.vue';

Vue.use(VueRouter);

const routes = [
  {    path: '/users', component: Users  },
  {    path: '/', component: Home  },
  {    path: '/home', component: Home  },
  {    path: '/loginhome', component: LoginHome  },
  {    path: '/userhome', component: UserHome  },
  {    path: '/player', component: Player  }
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

var vm = new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
